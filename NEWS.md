# GPGxE 0.0.1.9000

* Addition new models (factor analytic)
* Addition of a simulation phenotype function (GxE_sim_pheno)
* Addition of a global function for model evaluation

# GPGxE 0.0.0.9000

* Initial version


