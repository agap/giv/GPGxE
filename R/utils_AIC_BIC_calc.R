#' AIC_BIC_calc
#'
#' @description Calculates AIC and BIC
#'
#' @return The return value, if any, from executing the utility.
#'
#' @noRd

# logLik: log likelihood

# n: number of observations

# G_type: type of the g(e) term VCOV

# R_type: type of the err term VCOV

AIC_BIC_calc <- function(LogLik, Ne, Nobs, G_type, k = 1, R_type){

  # determination of genotype parameters
  if(G_type == "ID"){
    pG <- 1
  } else if(G_type == "DIAG"){
    pG <- Ne
  } else if (G_type == "MDs"){
    pG <- 2
  } else if (G_type == "MDe"){
    pG <- 1 + Ne
  } else if (G_type == "UN"){
    pG <- (Ne*(Ne + 1))/2
  } else if (G_type == "FA"){
    pG <- Ne*(Ne + k)
  }

  # determination of error parameters
  if(R_type == "ID"){
    pR <- 1
  } else if(R_type == "DIAG"){
    pR <- Ne
  }

  p <- pG + pR

  AIC <- (2*p) - (2*LogLik)
  BIC <- (p*log(Nobs)) - (2*LogLik)

  return(list(AIC = AIC, BIC = BIC))

}
