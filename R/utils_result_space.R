#' result_space
#'
#' @description Create some space to store GP_GxE results
#'
#' @return empty list with one element per model evaluated.
#'
#' @noRd

result_space <- function(n_iter, models = c('sommer', 'MTM'), Ne = 3){

  n_models <- length(models)
  res_list <- vector(mode = "list", length = n_models)
  names(res_list) <- models
  for(i in 1:n_models){
    res_list[[i]] <- vector(mode = "list", length = 7)
    names(res_list[[i]]) <- c("B", "SE", "SG", "Sgg","g_hat", "AIC", "BIC")
    res_list[[i]]$B <- res_list[[i]]$SE <- res_list[[i]]$SG <-
      matrix(NA, nrow = n_iter, ncol = Ne)
    res_list[[i]]$Sgg <- matrix(NA, nrow = n_iter, ncol = (Ne*(Ne-1))/2)
    res_list[[i]]$g_hat <- vector(mode = "list", length = n_iter)
    res_list[[i]]$AIC <- res_list[[i]]$BIC <- rep(NA, n_iter)
  }

  return(res_list)

}
