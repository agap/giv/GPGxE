
<!-- README.md is generated from README.Rmd. Please edit that file -->

# GPGxE

<!-- badges: start -->
<!-- badges: end -->

GPGxE is a collection of functions to realize genomic prediction in
multiple environments.

## Installation on your own computer

You can install the development version of GPGxE like that

``` r
# install the dependencies
install.packages("BGGE")
install.packages("dplyr")
install.packages("ggplot2")
install.packages("reshape2")
install.packages("sommer")
install.packages("stringr")

# Install MTM package. Other packages will be upgraded.
# If you do not want upgrade = FALSE
devtools::install_github("QuantGen/MTM", upgrade = TRUE)

# install the GPGxE package. If you do not want upgrade = FALSE
devtools::install_git("https://gitlab.cirad.fr/agap/giv/GPGxE.git",
                      upgrade = TRUE)
```

## Installation on Meso Cluster

If you install the development version of GPGxE on the cluster you could get trouble with package ugrading. So you can proceed like that

``` r
# install the dependencies
install.packages("BGGE")
install.packages("dplyr")
install.packages("ggplot2")
install.packages("reshape2")
install.packages("sommer")
install.packages("stringr")

# packages will not be upgraded. If you do want to upgrade (upgrade = TRUE)
devtools::install_github("QuantGen/MTM", upgrade = FALSE)

# install the GPGxE package. packages will not be upgraded.
# If you do want to upgrade (upgrade = TRUE)
devtools::install_git("https://gitlab.cirad.fr/agap/giv/GPGxE.git",
                      upgrade = FALSE)
```

## Example

This is a basic example to use the different models

``` r
library(GPGxE)
library(stringr)
library(sommer)
library(MTM)
library(BGGE)
library(reshape2)
library(ggplot2)

data("K", package = "GPGxE")
data("pheno", package = "GPGxE")

m <- GPGxE_model_computation(model = "BGGE_MM", pheno = pheno, K = K)
m <- GPGxE_model_computation(model = "BGGE_MDe", pheno = pheno, K = K)
m <- GPGxE_model_computation(model = "som_DIAG", pheno = pheno, K = K)
m <- GPGxE_model_computation(model = "som_UN", pheno = pheno, K = K)
m <- GPGxE_model_computation(model = "MTM_DIAG", pheno = pheno, K = K)
m <- GPGxE_model_computation(model = "MTM_UN", pheno = pheno, K = K)
```
